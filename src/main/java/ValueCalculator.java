import java.util.Arrays;

public class ValueCalculator {
    private int SIZE = 1_000_000;
    private int HALF_SIZE = SIZE / 2;
    private float masForFill[] = new float[SIZE];


    void doCalc() throws Exception {
        long start = System.currentTimeMillis();
        float[] firstArr = new float[HALF_SIZE];
        float[] secondArr = new float[HALF_SIZE];

        Arrays.fill(masForFill, 1);

        System.arraycopy(masForFill, 0, firstArr, 0, HALF_SIZE);
        System.arraycopy(masForFill, HALF_SIZE, secondArr, 0, HALF_SIZE);

        Thread t1 = new Thread(() -> doCalc(firstArr));
        Thread t2 = new Thread(() -> doCalc(firstArr));

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.arraycopy(firstArr, 0, masForFill, 0, HALF_SIZE);
        System.arraycopy(secondArr, 0, masForFill, HALF_SIZE, HALF_SIZE);
        System.out.println("Time:" + (System.currentTimeMillis() - start));
    }

    private void doCalc(float[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (float) (arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
        }
    }

}
